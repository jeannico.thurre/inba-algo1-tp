package s07;
import java.util.Arrays;

public class Ex1AboutMinimum {

  public static int min1(int[] t) {
    //TODO
    return min1(t, 0);
  }

  private static int min1(int[] t, int startIdx) {
    //TODO
    int toCompareElt = t[startIdx];

    if (startIdx == t.length-1)
      return toCompareElt;

    int minOfNextElts = min1(t, startIdx + 1);
    if (toCompareElt < minOfNextElts)
      return toCompareElt;
    else
      return minOfNextElts;
  }

  public static int min2(int[] t) {
    //TODO
    return min2(t, 0, t.length-1);
  }

  static int min2(int[] t, int startIdx, int endIdx) {
    //TODO
    int length = endIdx-startIdx;
    if (length == 0) {
      return t[startIdx];
    }

    int mid = startIdx + (length / 2);
    int minLeft = min2(t, startIdx, mid);
    int minRight = min2(t, mid + 1, endIdx);

    return minLeft < minRight ? minLeft : minRight;
  }

  //-------------------------------------------------------------------------
  
  @FunctionalInterface 
  interface MinFunction {
    int min(int[] t);
  }
  
  static void checkTestCase(int[] t, MinFunction mf) {
    int[] t1 = Arrays.copyOf(t, t.length);
    int observed = mf.min(t1);
    if(!Arrays.equals(t, t1)) 
      throw new IllegalStateException("The input array is modified!");
    int expected = Arrays.stream(t).min().getAsInt();
    if(observed != expected) 
      throw new IllegalStateException("Bad result: " + observed
          + " instead of " + expected + " in " + Arrays.toString(t));
  }
  
  private static void tinyMinTest() {
    int[][] samples = {
        {3, 4, 5},
        {5, 4, 3},
        {4, 3, 5},
        {-1, -9},
        {-9, -1},
        {8}
    };
    for(int[] u: samples) {
      checkTestCase(u, Ex1AboutMinimum::min1);
      checkTestCase(u, Ex1AboutMinimum::min2);
    }
    System.out.print("End of tiny test.");
  }

  public static void main(String [] args) {
    int[] t = {4, 3, 2, 6, 8, 7};
    System.out.println(min1(t));
    System.out.println(min2(t));
    System.out.println("tinyMinTest results : ");
    tinyMinTest();
  }

}
