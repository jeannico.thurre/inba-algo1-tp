package s11;
import java.util.BitSet;

public class BitSetOfShorts {
  final BitSet bs;
  static final short LOW  = Short.MIN_VALUE;
  static final short HIGH = Short.MAX_VALUE;
  // ------------------------------------------------------------
  static int indexFromElt(short e) {
    // TODO ...
    return e - LOW;
  }
  static short eltFromIndex(int i)   {
    // TODO ...
    return (short) (i + LOW);
  }
  // ------------------------------------------------------------
  public BitSetOfShorts()  {
    bs = new BitSet(); // or: new BitSet(1 + HIGH - LOW);
  }
  // ------------------------------------------------------------
  public void add(short e) {
    // TODO ...
    bs.set(indexFromElt(e));
  }
  public void remove(short e) {
    // TODO ...
     bs.clear(indexFromElt(e));
  }
  public boolean contains(short e) {
     // TODO ...
    return bs.get(indexFromElt(e));
  }
  public BitSetOfShortsItr iterator() {
    // TODO ...
    return new BitSetOfShortsItr() {
      int index = 0;
      @Override
      public boolean hasMoreElements() {
        return  index < bs.length()-1;
      }

      @Override
      public short nextElement() {
        index = bs.nextSetBit(++index);
        return eltFromIndex(index);
      }
    };
  }

  public void union(BitSetOfShorts s) {
    // TODO ...
    bs.or(s.bs);
  }
  public void intersection(BitSetOfShorts s) {
    // TODO ...
    bs.and(s.bs);
  }

  public int size() {
    // TODO ...
    return bs.cardinality();
  }

  public boolean isEmpty() {
    return bs.length() == 0;
  }

  public String toString() {
    String r = "{";
    BitSetOfShortsItr itr = iterator();
    if (isEmpty()) return "{}";
    r += itr.nextElement();
    while (itr.hasMoreElements()) {
      r += ", " + itr.nextElement();
    }
    return r + "}";
  }
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  public static void main(String [] args) {
    BitSetOfShorts a = new BitSetOfShorts();
    BitSetOfShorts b = new BitSetOfShorts();
    short[] ta = {-3, 5, 6, -3, 9, 9};
    short[] tb = {6, 7, -2, -3};
    int i;
    for (i=0; i<ta.length; i++) {
      a.add(ta[i]);
      System.out.println("" + a + a.size());
    }
    for (i=0; i<tb.length; i++) {
      b.add(tb[i]);
      System.out.println("" + b + b.size());
    }
    a.union(b);
    System.out.println("" + a + a.size());
  }
}
