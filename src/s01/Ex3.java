package S01;

public class Ex3 {
    public static void main(String[] args) {
        CharStack s = new CharStack();
        s.push('a');
        s.push('b');
        s.push('b');
        s.push('c');
        s.push('c');

        if(g(s, 'b') == 2) {
            System.out.println("g is understood");
        } else {
            System.out.println("g is not understood");
        }

        if(f("string").equals("gnirts")) {
            System.out.println("f is understood");
        } else {
            System.out.println("f is not understood");
        }
    }

    static int g(CharStack s, char e) {
        CharStack r = new CharStack();
        int c=0;
        // As long as s is not empty,
        // remove the most recent element from s,
        // and add it to r as the most recent element.
        while(!s.isEmpty())
            r.push(s.pop());
        // As long as r is not empty,
        // if the most recent element of r is e,
        // increment c
        // then remove the most recent element from r,
        // and add it to s as the most recent element.
        while(!r.isEmpty()) {
            if (r.top()==e) c++;
            s.push(r.pop());
        }
        // c = counts the number of elements of s that worth e
        return c;
    }
    static String f(String w) {
        String r="";
        CharStack s = new CharStack();
        // For each character of w,
        // the loop will add it to s.
        for(int i=0; i<w.length(); i++)
            s.push(w.charAt(i));
        // As long as s is not empty,
        // remove the most recent element from s,
        // and add it to the sequence of r.
        while(!s.isEmpty())
            r += s.pop();
        // r = characters of w written in reverse order.
        return r;
    }
}
