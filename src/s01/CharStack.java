package S01;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public class CharStack {
    private char[] buffer;
    private int topIndex;

    private int version;

    private static final int DEFAULT_SIZE = 10;

    public CharStack(int estimatedSize) {
        buffer = new char[estimatedSize];
        topIndex = -1;
        version = 1;
    }

    public CharStack() {
        this(DEFAULT_SIZE);
    }

    public boolean isEmpty() {
        return topIndex == -1;
    }

    // Precondition : The stack mustn't be empty when you call this method.
    public char top() {
        return buffer[topIndex];
    }

    // Precondition : The stack mustn't be empty when you call this method.
    public char pop() {
        char currentTopElement = buffer[topIndex];

        buffer[topIndex] = '\0';
        topIndex--;

        version++;

        return currentTopElement;
    }

    public void push(char x) {
        topIndex++;
        buffer[topIndex] = x;

        version++;

        if (topIndex == buffer.length - 1) {
            char[] bufferExtended = new char[buffer.length * 2];
            System.arraycopy(buffer, 0, bufferExtended, 0, buffer.length);
            buffer = bufferExtended;
        }
    }

    public Enumeration<Character> topDownTraversal() {
        Enumeration<Character> enumeration = new Enumeration<>() {
            int index = topIndex + 1;
            int versionAtStart = version;

            @Override
            public boolean hasMoreElements() {
                if (versionAtStart != version) {
                    throw new ConcurrentModificationException();
                }
                return index - 1 >= 0;
            }

            @Override
            public Character nextElement() {
                if (versionAtStart != version) {
                    throw new ConcurrentModificationException();
                }
                if (!hasMoreElements()) {
                    throw new NoSuchElementException();
                }
                return buffer[--index];
            }
        };

        return enumeration;
    }
}

