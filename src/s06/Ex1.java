package s06;

public class Ex1 {
    public static void main(String[] args) {
        SetOfShorts s = new SetOfShorts();
        s.add((short) 1);
        s.add((short) 5);
        s.add((short) 6);
        s.add((short) 10);
        s.add((short) 3);
        s.add((short) 2);

        System.out.println(z(s, 4));
    }

    public static SetOfShorts z(SetOfShorts s, int x) {
        SetOfShorts res = new SetOfShorts();
        SetOfShortsItr itr = s.iterator();
        while (itr.hasMoreElements()) {
            short i=itr.nextElement();
            if (i<x) res.add(i);
        }
        return res;
    }
}
