package s06;

import java.util.Arrays;
import java.util.Random;

public class RandomTable {
  static Random r = new Random();
  // ------------------------------------------------------------
  // PRE: m <= n, m >= 0
  // Returns: a sorted array with randomly chosen distinct elements in [0..n-1]
  public static short[] randomTable(short m, short n) {
    // TODO
    // PSEUDOCODE : 
    //     create an empty set
    //     while the set has less than m elements
    //       add to the set a randomly chosen element  
    //     with an iterator, copy the entire set into an array
    //     sort the array

    SetOfShorts set = new SetOfShorts();

    // Insert random value into the set.
    while (set.size() < m) {
      set.add((short) r.nextInt(n));
    }

    // Copy elements into the array
    short[] result = new short[m];
    SetOfShortsItr itr = set.iterator();

    int i = 0;
    while (itr.hasMoreElements())
      result[i++] = itr.nextElement();

    // Sorting
    Arrays.sort(result);
    return result;
  }
  // ------------------------------------------------------------
  static void testRandomTable(short m, short n, int nRepetitions) {
    String msg = "";
    for(int k=0; k<nRepetitions; k++) {
      short[] t = randomTable(m, n);
      msg = String.format("m=%d n=%d res=%s", m, n, Arrays.toString(t));
      if (m != t.length)
        throw new RuntimeException("Size of array is not correct: " + msg);
      if(m == 0) return;
      if (t[0]<0 || t[t.length-1] >= n)
        throw new RuntimeException("Elements must be in [0..n[" + msg);
      for (int i = 0; i < t.length - 1; i++) {
        if (t[i] >= t[i + 1])
          throw new RuntimeException(
              "Array should be sorted and contain distinct numbers: " + msg);
      }
    }
    System.out.println("One sample: "+ msg); 
    System.out.println("\nTest passed successfully !");
  }
  // ------------------------------------------------------------
  public static void main(String [] args) {
    short m = 10; 
    short n = 50;
    if (args.length == 2) {
      m=Short.parseShort(args[0]);
      n=Short.parseShort(args[1]);
    }
    int nRepetitions = 100;
    testRandomTable(m, n, nRepetitions);
  }
}
