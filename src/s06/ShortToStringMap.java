package s06;

public class ShortToStringMap {
  short[] keys;
  String[] imgs;
  final int INITIAL_ARRAY_LENGTH = 10;

  private int size;
  // TODO ...
  //------------------------------
  //  Private methods
  //------------------------------

  // Could be useful, for instance :
  // - one method to detect and handle the "array is full" situation
  // - one method to locate a key in the array
  //   (to be called from containsKey(), put(), and remove())

  boolean isArrayFull() {
    return size >= keys.length;
  }

  int getKeyIdx(short key) {
    for (int i = 0; i < size; i++) {
      if (key == keys[i])
        return i;
    }

    //if not found, returns -1
    return -1;
  }

  //------------------------------
  //  Public methods
  //------------------------------
  public ShortToStringMap() {
    // TODO ...
    keys = new short[INITIAL_ARRAY_LENGTH];
    imgs = new String[INITIAL_ARRAY_LENGTH];

    size = 0;
  }

  // adds an entry in the map, or updates it
  public void put(short key, String img) {
    // TODO ...
    if(!containsKey(key)) {
      if (isArrayFull()) {
        short[] newKeys = new short[keys.length * 2];
        String[] newImgs = new String[keys.length * 2];
        System.arraycopy(keys, 0, newKeys, 0, keys.length);
        System.arraycopy(imgs, 0, newImgs, 0, keys.length);

        keys = newKeys;
        imgs = newImgs;
      }

      keys[size] = key;
      imgs[size] = img;
      size++;
    } else {
      imgs[getKeyIdx(key)] = img;
    }
  }

  // returns null if !containsKey(key)
  public String get(short key) {
    // TODO ...
    if(containsKey(key)){
      return imgs[getKeyIdx(key)];
    }
    return null;
  }

  public void remove(short e) {
    // TODO - ...
    if(containsKey(e)){
      int eltIndex = getKeyIdx(e);
      keys[eltIndex] = keys[size-1];
      imgs[eltIndex] = imgs[size-1];
      size--;
    }
  }

  public boolean containsKey(short k) {
    // TODO ...
    return getKeyIdx(k) != -1;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public int size() {
    return size;
  }

  public ShortToStringMapItr iterator() {
    // TODO ...
    return new ShortToStringMapItr() {
      int positionIdx = 0;
      @Override
      public boolean hasMoreKeys() {
        return positionIdx < size;
      }

      @Override
      public short nextKey() {
        return keys[positionIdx++];
      }
    };
  }

  // a.union(b) :        a becomes "a union b"
  //  values are those in b whenever possible
  public void union(ShortToStringMap m) {
    // TODO ...
    for (int i = 0; i < m.size; i++) {
      put(m.keys[i], m.imgs[i]);
    }
  }

  // a.intersection(b) : "a becomes a intersection b"
  //  values are those in b
  public void intersection(ShortToStringMap s) {
    // TODO ...
    for (int i = 0; i < size; i++) {
      if (s.containsKey(keys[i])) {
        int elmtIdxS = s.getKeyIdx(keys[i]);
        imgs[i] = s.imgs[elmtIdxS];
      } else {
        remove(keys[i]);
        i--;
      }
    }
  }

  // a.toString() returns all elements in
  // a string like: {3:"abc",9:"xy",-5:"jk"}
  @Override public String toString() {
    // TODO ...
    String out = "{";

    for (int i = 0; i < size; i++) {
      out +=  keys[i]
              + ":\""
              + imgs[i]
              + "\"";
      if (i != keys.length-1)
        out += ",";
    }

    out += "}";
    return out;
  }
}
