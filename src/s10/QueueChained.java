package s10;
import java.util.Random;

public class QueueChained<T> {
  //======================================================================
  private static class QueueNode<T> {
    final T elt;
    QueueNode<T> next = null;
    // ----------
    QueueNode(T elt) { this.elt = elt; }
  }
  // ======================================================================
  private QueueNode<T> front;
  private QueueNode<T> back;
  // ------------------------------
  public QueueChained() { }

  public void enqueue(T elt) {
    QueueNode<T> enqueued = new QueueNode<>(elt);
    if (back == null && front == null)
      front = enqueued;
    else
      back.next = enqueued;
    back = enqueued;
  }

  public boolean isEmpty() {
    return back==null;
  }

  // PRE : !isEmpty()
  public T consult()     {
    return front.elt;
  }

  // PRE : !isEmpty()
  public T dequeue() {
    T e = front.elt;
    if (front == back) {
      back = null; front = null;
    } else {
      front = front.next;
    }
    return e;
  }
}
