package s10;

public class ObjQueue {
  //======================================================================
  private static class QueueNode {
    final Object elt;
    QueueNode next = null;
    // ----------
    QueueNode(Object elt) { this.elt = elt; }
  }
  // ======================================================================
  private QueueNode front;
  private QueueNode back;
  // ------------------------------
  public ObjQueue() { }

  public void enqueue(Object elt) {
    // TODO ...
    QueueNode enqueued = new QueueNode(elt);
    if (back == null && front == null)
      front = enqueued;
    else
      back.next = enqueued;
    back = enqueued;
  }

  public boolean isEmpty() {
    return back==null;
  }

  // PRE : !isEmpty()
  public Object consult()     {
    return front.elt;
  }

  // PRE : !isEmpty()
  public Object dequeue() {
    Object e = front.elt;
    if (front == back) {
      back = null; front = null;
    } else {
      front = front.next;
    }
    return e;
  }
}
