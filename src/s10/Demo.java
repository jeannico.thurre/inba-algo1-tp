package s10;
public class Demo {
  static void demo(int n) {
    IntQueueChained f; 
    int i, sum=0;
    f = new IntQueueChained();
    for (i=0; i<n; i++)
      f.enqueue(i);
    while(! f.isEmpty()) 
      sum = sum + f.dequeue();
    System.out.println(sum);
  }

  //Made by JN
  static void demoQueueChained(int n) {
    QueueChained<Integer> f;
    int i, sum=0;
    f = new QueueChained<Integer>();
    for (i=0; i<n; i++)
      f.enqueue(i);
    while(! f.isEmpty())
      sum = sum + f.dequeue();
    System.out.println(sum);
  }

  static void demoObjQueue(int n) {
    ObjQueue f;
    int i, sum=0;
    f = new ObjQueue();
    for (i=0; i<n; i++)
      f.enqueue(i);
    while(! f.isEmpty())
      sum = sum + (int) f.dequeue();
    System.out.println(sum);
  }

  public static void main(String[] args) {
    demo(10);
    demoQueueChained(10);
    demoObjQueue(10);
  }
}
