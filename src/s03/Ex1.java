package S03;

public class Ex1 {
    public static void main(String[] args) {

    }

    static int g(List l, int i) {
        ListItr li = new ListItr(l);

        // for i times
        for(int k=0; k<i; k++)
            //  move the iterator to the next
            li.goToNext();

        // then read the value
        return li.consultAfter();
    }

    static boolean f(List l, int e) {
        ListItr li = new ListItr(l);
        // until we're at the end
        while(!li.isLast()) {
            // if the next element is equivalent to e
                // returns true
            if (li.consultAfter()==e) return true;
            // move to the iterator to the next
            li.goToNext();
        }
        return false;
    }
}
