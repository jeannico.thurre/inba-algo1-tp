package S03;

public class AmStramGram {
  
  // "Josephus problem" with persons numbered 1..n
  // Removes every k-th persons (ie skip k-1 persons)
  // PRE: n>=1, k>=1
  // RETURNS: the survivor
  // Example: n=5, k=2: 
  //   '12345 → 1'2345 → 1'(2)345 → 1'345 
  //          → 13'45  → 13'(4)5  → 13'5  
  //          → 135'   → '(1)35   → '35 
  //          → 3'5    → 3'(5)    → 3'  ===> survivor: 3
  public static int winnerAmStramGram(int n, int k) {
    List list = new List();
    ListItr li = new ListItr(list);
    int a = n;
    // The following loop inserts the values in the table (number of people, length of the jump)
    for (int i = 1; i < (n+1); i++) {
      li.insertAfter(i);
      li.goToNext();
    }
    // ListItr moves by the jump number k, deletes, and continues until there is one person left.
    while (n > 1) {
      if (a == n) {li.goToFirst();}
      if (li.succ == null) {
        li.goToFirst();
      }
      for (int j = 0; j < (k-1); j++) {
        li.goToNext();
        if (li.succ == null) {
          li.goToFirst();
        }
      }
      li.removeAfter();
      n--;
    }
    li.goToFirst();
    return li.consultAfter();
  }
  // ----------------------------------------------------------  
  static void josephusDemo(int n, int k) {
    System.out.printf("n=%d, k=%d : Survivor is %d\n", n, k, winnerAmStramGram(n, k));
  }
  
  static void josephusTest() {
    int[][] sol = {
        {1, 1, 1, 1, 1, 1, 1},  // n=1
        {2, 1, 2, 1, 2, 1, 2},  // n=2
        {3, 3, 2, 2, 1, 1, 3},  // n=3
        {4, 1, 1, 2, 2, 3, 2},  // n=4
        {5, 3, 4, 1, 2, 4, 4},  // n=5
        {6, 5, 1, 5, 1, 4, 5},  // n=6
        {7, 7, 4, 2, 6, 3, 5}   // n=7
    //   1  2  3  4  5  6  7 <--- k
    };
    for(int n=1; n<8; n++)
      for(int k=1; k<8; k++) {
        int r = winnerAmStramGram(n, k);
        int expected = sol[n-1][k-1];
        if(r != expected) {
          String msg = String.format("mismatch: n=%d, k=%d, expected=%d, observed=%d%n",
              n, k, expected, r);
          throw new AssertionError(msg);          
        }
    }
    System.out.println("Josephus test passed successfully!");
  }
  
  public static void main(String[] args) {
    int n=5, k=2;  
    if (args.length == 2) {
      n = Integer.parseInt(args[0]);
      k = Integer.parseInt(args[1]);
    }
    josephusDemo(n, k);
    josephusTest();
  }
}
