package S03;

// When isFirst(), it is forbidden to call goToPrev()
// When isLast(),  it is forbidden to call goToNext() 
// When isLast(),  it is forbidden to call consultAfter(), or removeAfter()
// For an empty list, isLast()==isFirst()==true
// For a fresh ListItr, isFirst()==true
// Using multiple iterators on the same list is allowed only 
// if none of them modifies the list

public class ListItr {
  final List list;
  ListNode pred, succ;
  // ----------------------------------------------------------
  public ListItr( List anyList ) {
    list = anyList; 
    goToFirst();   
  }

  public void insertAfter(int e) {
    ListNode insertedNode = new ListNode(e, pred, succ);
    if (isFirst()) {
      list.first = insertedNode;
    } else {
        pred.next = insertedNode;
    }

    if (isLast()) {
      list.last = insertedNode;
    } else {
      succ.prev = insertedNode;
    }

    succ = insertedNode;

    list.size++;
  }

  public void removeAfter() {
    boolean isBeforeTheLast = succ.next == null;

    if (!isFirst() && !isBeforeTheLast) {
      succ = succ.next;
      succ.prev = pred;
      pred.next = succ;
    }

    //if succ is the last node
    if(isBeforeTheLast) {
      if(!isFirst()) {
        pred.next = null;
      }
      succ = null;
      list.last = pred;
    }

    if (isFirst()) {
      if(!isBeforeTheLast) {
        succ = succ.next;
        succ.prev = null;
      }
      list.first = succ;
    }

    list.size--;
  }

  public int  consultAfter() {
    return succ.elt;
  }
  public void goToNext() {
    pred = succ; 
    succ = succ.next; 
  }
  public void goToPrev() {
    succ = pred;
    pred = pred.prev; 
  }
  public void goToFirst() { 
    succ = list.first; 
    pred = null;
  }
  public void goToLast() { 
    pred = list.last;  
    succ = null;
  }
  public boolean isFirst() { 
    return pred == null;
  }
  public boolean isLast() { 
    return succ == null; 
  }
}

