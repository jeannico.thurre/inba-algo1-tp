package s09;
public class IntStack {
  private int[] buf;     // erreur d
  private int top;  // growing downwards
  public IntStack() { this(10); }
  public IntStack(int initialCapacity) {
    assert initialCapacity > 0; //Added by JN
    buf = new int[initialCapacity]; 
    top = initialCapacity;
  } 
  public boolean isEmpty() {
    return top == buf.length;//-1; //erreur a
  }
  public int pop() {
    assert !isEmpty(); //Added by JN
    int e = buf[top];  
    top++; 
    return e;
  }
  public void push(int x) { 
    checkSize();
    top--; //erreur b
    buf[top] = x; //buf[x] = top; // erreur d

    assert buf[top] == x;  //Added by Jn
  }
  private void checkSize() {
    int n = buf.length;
    if (top == 0) { 
      int[] t = new int[2*n];
      for (int i=0; i<n; i++) 
        t[i+n] = buf[i];
      buf = t;
      top = n;
    }
  }
  // ----------------------------------------------------------------------
  static boolean isBuggy01() {
    IntStack s = new IntStack();
    s.push(9);
    if (s.pop() != 9) return true;
    if (!s.isEmpty()) return true;
    return false;
   }
  
  static boolean isBuggy02() {
    IntStack s = new IntStack();
    s.push(9); s.push(5);
    if (s.isEmpty()) return true;
    return false;
   } 
  
  static boolean isBuggy03() {
    IntStack s = new IntStack();
    s.push(6); s.push(5); 
    s.pop();   s.push(8);
    if (s.pop() != 8) return true;
    if (s.isEmpty())  return true;
    return false;
   }

  // ----------------------------------------------------------------------
  public static void main(String [] args) {
    System.out.println(isBuggy01());
    System.out.println(isBuggy02());
    System.out.println(isBuggy03());
    System.out.println(isBuggy04());
    System.out.println(isBuggy05());
  }

  //DONE BY JN
  static boolean isBuggy04() { // to detecting error c
    IntStack s = new IntStack();
    s.push(50);
    if (s.pop() != 50) return true;
    if (!s.isEmpty()) return true;
    return false;
  }
  //DONE BY JN
  static boolean isBuggy05() { // to detecting error d
    IntStack s1 = new IntStack();
    IntStack s2 = new IntStack();
    s2.push(50);
    if (!s1.isEmpty()) return true;
    return false;
  }
  //DONE BY JN
  static boolean s() { // to detecting error d
    IntStack s1 = new IntStack();
    IntStack s2 = new IntStack();
    s2.push(50);
    if (!s1.isEmpty()) return true;
    return false;
  }
}
