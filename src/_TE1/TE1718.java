package _TE1;

public class TE1718 {
    public static void main(String[] args) {
        ex1415_b(15);
    }
    static void ex1(long n){
        long i=0, j=0, r=0;
        while (i < n*n) {
            r++;
            j++;
            if (j>=n) {
                j=0;
                i++;
                r++; r++; r++;
            }
        }
        System.out.println(r);
    }

    static void exjsp_a(long n){
        long i, j, r=0;
        for (i = 0; i < n*n; i++) {
            if (r >= 77)
                for(j=0; j < n; j++)
                    r++;
            else
                r++;

        }

        System.out.println(r);
    }

    static void ex1415_3(long n) {
        long r=0, k=1;
        for (int i = 0; i < n; i++)
            k=2*k;
        for(long i=k; i>1; i=i-6)
            r++;
        for(long i=0; i<n; i++)
            if(10*i >= i*i)
                r++;

        System.out.println(r);
    }

    static void ex1415_b(long n) {
        long i=1, j=1, r=0;
        while(j<n){
            i++;
            if (i*i>=n) {
                j=3*j; i=1;}
            r++;
        }
        System.out.println(r);
    }
}
