package s08;
public class RLExos {
  //----------------------------------------------------------------
  //--- Exercises, S08 :
  //----------------------------------------------------------------
  /* append: adds an element at the end of the list.
             append("abcd", 'z') gives "abcdz"                    */

  public static CharRecList append(CharRecList l, char e) {
    // TODO...
    if (l.isEmpty())
      return l.withHead(e);

    // on descend jusqu'à la tail null, on donne e comme tête puis on remonte et remettant la tête
    return append(l.tail(), e).withHead(l.head());
  }
  //----------------------------------------------------------------
  /* concat: concatenates two lists.
             concat("abcd", "xyz") gives "abcdxyz"               */
  public static CharRecList concat(CharRecList l, CharRecList r) {
    // TODO...
    if (l.isEmpty())
      return r;
    else if (l.tail().isEmpty())
      return r.withHead(l.head());
    return concat(l.tail(), r).withHead(l.head());
  }
  //----------------------------------------------------------------
  /* replaceEach : changes every occurrence of a certain element.
              replaceEach("bcabcaxy", 'a', 'b') gives "bcbbcbxy"  */
  public static CharRecList replaceEach(CharRecList l, char old, char by) {
    // TODO...
    if (l.isEmpty())
      return l;

    char head = l.head()==old ? by : l.head();
    return replaceEach(l.tail(), old, by).withHead(head);
  }
  //----------------------------------------------------------------
  /* consultAt: returns the element at index i (counting from zero).
                consultAt("abcde", 3) gives 'd'                   */
  public static char consultAt(CharRecList l, int index) {
    // TODO...
    if (index == 0)
      return l.head();

    char c = consultAt(l.tail(), --index);
    return c;
  }
  //----------------------------------------------------------------
  /* isEqual: whether both lists have the same content (in same order).
              isEqual("abc", "abc")  is true
              isEqual("abc", "bca")  is false
              isEqual("abc", "abcd") is false                     */
  public static boolean isEqual(CharRecList a, CharRecList b) {
    // TODO...
    if (a.isEmpty() && b.isEmpty())
      return true;
    else if (a.isEmpty() || b.isEmpty())
      return false;
    else if (a.head()!=b.head()) {
      return false;
    }
    boolean bool = isEqual(a.tail(), b.tail());
    return bool;
  }
  //----------------------------------------------------------------
  /* isSuffix: whether suff is a suffix of l.
               isSuffix("abcd", "bcd")  is true
               isSuffix("abcd", "bc")   is false
               isSuffix("abcd", "")     is true
               isSuffix("abcd", "abcd") is true                   */
  public static boolean isSuffix(CharRecList l, CharRecList suff) {
    // TODO...
    if (suff.isEmpty())
      return true;
    else if (l.isEmpty())
      return false;
    else if (isEqual(l, suff))
      return true;
    else
      return isSuffix(l.tail(), suff);
  }

  //----------------------------------------------------------------
  //--- Examples from course slides:
  //----------------------------------------------------------------
  public static int sizeOf(CharRecList l) {
    if (l.isEmpty())
      return 0;
    return 1 + sizeOf(l.tail());
  }
  //----------------------------------------------------------------
  public static CharRecList inverse(CharRecList l) {
    if (l.isEmpty())
      return l;
    return append(inverse(l.tail()), l.head());
  }
  //----------------------------------------------------------------
  public static boolean isMember(CharRecList l, char e) {
    if (l.isEmpty())
      return false;
    if (e == l.head())
      return true;
    return isMember(l.tail(), e);
  }
  //----------------------------------------------------------------
  public static CharRecList smaller(CharRecList l, char e) {
    if (l.isEmpty())
      return l;
    if ( l.head() < e )
      return smaller(l.tail(), e).withHead(l.head());
    return smaller(l.tail(), e);
  }
  //----------------------------------------------------------------
  public static CharRecList greaterOrEqual(CharRecList l, char e) {
    if (l.isEmpty())
      return l;
    if ( l.head() < e )
      return greaterOrEqual(l.tail(), e);
    return greaterOrEqual(l.tail(), e).withHead(l.head());
  }
  //----------------------------------------------------------------
  public static CharRecList quickSort(CharRecList l) {
    CharRecList left, right;
    if (l.isEmpty())
      return l;
    left  =        smaller(l.tail(), l.head());
    right = greaterOrEqual(l.tail(), l.head());
    left  = quickSort(left);
    right = quickSort(right);
    return concat(left, right.withHead(l.head()));
  }
  //----------------------------------------------------------------
  //----------------------------------------------------------------
  //----------------------------------------------------------------
  public static void main(String [] args) {
    CharRecList l = CharRecList.EMPTY_LIST;
    CharRecList m = CharRecList.EMPTY_LIST;
    l = l.withHead('c').withHead('d').withHead('a').withHead('b');
    m = m.withHead('t').withHead('u').withHead('v');

    System.out.println("list l : "+ l);
    System.out.println("list m : "+ m);

    // Tests
    // l : badc et m : vut
    if (!"badcz".equals(append(l,'z').toString()))               throw new RuntimeException("append doesn't work");
    System.out.println("append() works");

    if (!"badcvut".equals(concat(l,m).toString()))                  throw new RuntimeException("concat doesn't work");
    System.out.println("concat() works");

    if (!"bzdc".equals(replaceEach(l,'a','z').toString()))  throw new RuntimeException("replaceEach doesn't work");
    System.out.println("replaceEach() works");

    if ('d' != consultAt(l,2))                                throw new RuntimeException("consultAt doesn't work");
    System.out.println("consultAt() works");

    if (isEqual(l, m))                                              throw new RuntimeException("isEqual doesn't work");
    if (isEqual(l, CharRecList.EMPTY_LIST.withHead('b')))        throw new RuntimeException("isEqual doesn't work");
    if (!isEqual(l, l))                                             throw new RuntimeException("isEqual doesn't work");
    System.out.println("isEqual() works");

    if (isSuffix(l, m))                                             throw new RuntimeException("isSuffix doesn't work");
    if (isSuffix(l, CharRecList.EMPTY_LIST.withHead('d')))       throw new RuntimeException("isSuffix doesn't work");
    if (!isSuffix(l, CharRecList.EMPTY_LIST.withHead('c').withHead('d')))       throw new RuntimeException("isSuffix doesn't work");
    System.out.println("isSuffix() works");
    // End tests

    System.out.println( "quickSort(l) : "+           quickSort(l)   );
    System.out.println( "append(l,'z') : "+          append(l,'z')  );
    System.out.println( "concat(l,m) : "+            concat(l,m)    );
    System.out.println( "replaceEach(l,'a','z') : "+ replaceEach(l,'a','z') );
    System.out.println( "consultAt(l,2) : "+         consultAt(l,2) );
    //...
  }
}
