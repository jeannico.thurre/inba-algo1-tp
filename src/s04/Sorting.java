package s04;
public class Sorting {
  public static void main(String [] args) {
    int[] t = {4, 2, 3, 6, 8, 7};
    int[] u = {2, 3, 4, 6, 7, 8};
    //insertionSort(t);
    //selectionSort(t);
    //shellSort(t);
    for (int i=0; i<t.length; i++) {
      if (t[i] != u[i]) {
        System.out.println("Something is wrong...");
        return;
      }
    }
    int[] v = {5};
    //insertionSort(t);
    //selectionSort(t);
    //shellSort(t);
    int[] w = {};
    //insertionSort(t);
    //selectionSort(t);
    //shellSort(t);
    System.out.println("\nMini-test passed successfully...");
  }
  //------------------------------------------------------------
  public static void selectionSort(int[] a) {
    for (int toSwapIdx=0; toSwapIdx<a.length; toSwapIdx++) {
      int minIdx = toSwapIdx;
      //loop through to the search range
      for (int srchIdx=toSwapIdx; srchIdx<a.length; srchIdx++) {
        if (a[srchIdx] < a[minIdx]) minIdx = srchIdx;
      }

      //swap values
      {
        // store elm to swap
        int toSwapTmpElm = a[toSwapIdx];
        // insert min elmt
        a[toSwapIdx] = a[minIdx];
        // replace with elm
        a[minIdx] = toSwapTmpElm;
      }
    }
  }
  //------------------------------------------------------------
  public static void shellSort(int[] a) {
    //calculate initial k based on a.length
    int k = 1;
    while (1+3*k < a.length) {
      k = 1+3*k;
    }

    while (k >= 1){
      for (int toInsrIdx = k; toInsrIdx < a.length; toInsrIdx++) {
        int toInsrElm = a[toInsrIdx];

        int moveItr = toInsrIdx;
        while (moveItr - k >= 0 && a[moveItr] < a[moveItr-k]) {
          //switch elements
          a[moveItr] = a[moveItr-k];
          moveItr -= k;
        }
        a[moveItr] = toInsrElm;
      }

      k = (k-1)/3;
    }
  }
  //------------------------------------------------------------
  public static void insertionSort(int[] a) {
    int i, j, v;

    for (i=1; i<a.length; i++) {
      v = a[i];          // v is the element to insert
      j = i;
      while (j>0 && a[j-1] > v) {
        a[j] = a[j-1];   // move to the right
        j--;
      }
      a[j] = v;          // insert the element
    }
  }
  //------------------------------------------------------------
}
