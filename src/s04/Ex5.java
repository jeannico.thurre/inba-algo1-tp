package s04;

import S03.List;
import S03.ListItr;

public class Ex5 {
    public static void main(String[] args) {
        List l=new List();
        ListItr li = new ListItr(l);
        int[] t = {4,0,3,9,2,1,8};
        for(int i=0; i<t.length; i++) {
            li.insertAfter(t[i]); li.goToNext();
        }
        li.goToFirst();
        li.goToNext();
        li.goToNext();
        System.out.println(f(li));
    }

    static int f(ListItr li) {
        int m = li.consultAfter();
        int i=1, j=1;
        li.goToNext();
        //for each element from actual position to the end
        while(!li.isLast()) {
            i++; j++;
            int e=li.consultAfter();
            //store the smallest element to m
            if (e<m) {m=e; i=1;}
            li.goToNext();
        }

        //removes the smallest element
        while(i>0) {li.goToPrev(); i--; j--;}
        li.removeAfter();
        //go to initial position
        while(j>0) {li.goToPrev(); j--;}
        return m;
    }
}
