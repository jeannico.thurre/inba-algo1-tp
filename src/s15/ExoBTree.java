package s15;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class ExoBTree {

  public static String breadthFirstQ(BTNode t) {
    int height = height(t);
    String course = "";

    for (int i = 0; i < height; i++) {
      course += visitLevel(t, i);
    }

    return course;

  }

  public static int size(BTNode t) {
    // TODO ...
    if (t == null) return 0;
    return 1 + size(t.left) + size(t.right);
  }

  public static int height(BTNode t) {
    // TODO ...
    if (t == null) return 0;
    return 1 + Math.max(height(t.left), height(t.right));
  }

  public static String breadthFirstR(BTNode t) {
    // TODO ...
    int height = height(t);
    String course = "";

    for (int i = 0; i < height; i++) {
      course += visitLevel(t, i);
    }

    return course;
  }

  public static String visitLevel(BTNode t, int level) {
    // TODO ...
    if (t == null) return "";
    if (level == 0) return " " + t.elt;

    return visitLevel(t.left, level - 1) + visitLevel(t.right, level - 1);
  }


  public static void depthFirst(BTNode t) {
    if (t==null) return;
    System.out.print(" "+t.elt);
    depthFirst(t.left );
    depthFirst(t.right);
  }

  public static void rotateRight(BTNode y) {
    // TODO ...
    BTNode x = y.left;
    BTNode p = y.parent;
    BTNode b = x.right;

    // move x as parent of y
    x.parent = p;
    x.right = y;
    y.parent = x;
    y.left = b;

    // move b
    if (b != null) b.parent = y;

    //move p
    if (p == null) return;
    if (p.left == y)
      p.left = x;
    else
      p.right = x;
  }
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  private static Random rnd = new Random();
  // ------------------------------------------------------------
  public static BTNode rndTree(int size) {
    if (size==0) return null;
    BTNode root = new BTNode(Integer.valueOf(0), null, null, null);
    BTNode t=root;
    boolean isLeft;
    for(int i=1; i<size; i++) {
      t=root;
      while(true) {
        isLeft=rnd.nextBoolean();
        if (isLeft){
          if (t.left ==null) break;
          t=t.left;
        }else{
          if (t.right==null) break;
          t=t.right;
        }
      }
      BTNode newLeaf = new BTNode(Integer.valueOf(i), null, null, t);
      if (isLeft) t.left =newLeaf;
      else        t.right=newLeaf;
    }
    return root;
  }
  // ------------------------------------------------------------
  public static void main(String[] args) {
    int nbOfNodes = 10;
    BTNode t = rndTree(nbOfNodes);
    System.out.println("Tree:" +t);
    System.out.println(t.toReadableString());
    System.out.println("\nDepth first (recursive), preorder:");
    depthFirst(t); 
    System.out.println("\nBreadth first:");
    System.out.println(breadthFirstQ(t));
    System.out.println("\nBreadth first bis:");
    System.out.println(breadthFirstR(t));
    System.out.println("\nSize:" + size(t));
    System.out.println("\nHeight:" + height(t));
  }
  // ------------------------------------------------------------
}

