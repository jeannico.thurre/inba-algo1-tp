package s12;
import java.util.Random;
public class RndTriangle {
  public static void main(String [] args) {
    int nbOfExperiments = 100000;
    Random r = new Random();
    if (args.length>0) nbOfExperiments = Integer.parseInt(args[0]);
    System.out.println(rndTriangleAvgArea(r, nbOfExperiments));
  }
  //============================================================
  public static double rndTriangleAvgArea(Random r, int nbOfExperiments) {
    // TODO
    double sum = 0.0;
    for (int i = 0; i < nbOfExperiments; i++) {
      double  x1 = r.nextDouble(), x2 = r.nextDouble(), x3 = r.nextDouble(),
              y1 = r.nextDouble(), y2 = r.nextDouble(), y3 = r.nextDouble();
      sum += 0.5*Math.abs(x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2));
    }
    return sum/nbOfExperiments;
  }
}