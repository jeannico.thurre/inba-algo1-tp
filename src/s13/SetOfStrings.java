package s13;
import java.util.BitSet;

public class SetOfStrings {
  static final int DEFAULT_CAPACITY = 5;
  int      crtSize;
  // three "parallel arrays" (alternative: one array HashEntry[], 
  // with String/int/boolean fields)
  String[] elt;
  int[]    total;
  BitSet   busy;
  // ------------------------------------------------------------
  public SetOfStrings ()  {
    this(DEFAULT_CAPACITY);
  }

  public SetOfStrings(int initialCapacity)  {
    initialCapacity = Math.max(2, initialCapacity);
    elt   = new String[initialCapacity];
    total = new    int[initialCapacity];
    busy  = new BitSet(initialCapacity);
    crtSize = 0;
  }

  int capacity() {
    return elt.length;
  }

  // Here is the hash function :
  int hashString(String s) {
    int h = s.hashCode() % capacity();
    if (h<0) h=-h;
    return h;
  }

  // PRE: table is not full
  // returns the index where element e is stored, or, if
  // absent, the index of an appropriate free cell 
  // where e can be stored
  int targetIndex(String e) {
    // TODO
    int eltIdx = hashString(e);
    int totalIdx = total[eltIdx];

    // look every element with the same hash
    while (totalIdx > 0) {
      // an element with this hash exists
      if (busy.get(eltIdx)) {
        // element exists
        if (e.equals(elt[eltIdx]))
          return eltIdx;

        // the same hash but not the same value
        if (hashString(e) == hashString(elt[eltIdx]))
          totalIdx--;
      }

      // search in the next cells
      eltIdx++;
      if (eltIdx >= elt.length) {
        eltIdx = 0;
      }

      // CORRECTION
      // Ajout modulo pour les eltIdx++
      // Variable pour le hashstring
    }

    // if not exists, search the next free cell
    eltIdx = hashString(e);
    while (busy.get(eltIdx)) {
      eltIdx++;
      if (eltIdx >= elt.length)
        eltIdx = 0;
    }

    return eltIdx;
  }

  public void add(String e) {
    if (crtSize*2 >= capacity()) 
      doubleTable();

    // TODO
    if (!contains(e)) {
      int idx = targetIndex(e);
      crtSize++;
      elt[idx] = e;
      total[hashString(e)]++;
      busy.set(idx);
    }

    // CORRECTION
    if (contains(e)) return;

    int idx = targetIndex(e);
    crtSize++;
    elt[idx] = e;
    total[hashString(e)]++;
    busy.set(idx);
  } 

  private void doubleTable() {
    // TODO
    SetOfStrings newSet = new SetOfStrings(elt.length * 2);
    for (String elt : arrayFromSet())
      newSet.add(elt);

    this.crtSize = newSet.crtSize;
    this.elt = newSet.elt;
    this.total = newSet.total;
    this.busy = newSet.busy;
  }

  public void remove(String e) {
    int i = targetIndex(e);
    if (!busy.get(i)) return; // elt is absent
    int h = hashString(e);
    total[h]--;
    elt[i]=null;
    busy.clear(i); 
    crtSize--;
  }

  public boolean contains(String e) {
    return busy.get(targetIndex(e));
  }

  public SetOfStringsItr iterator() {
    // TODO
    return new SetOfStringsItr(this);
  }

  private class SetOfStringsItr implements s13.SetOfStringsItr {
    // TODO
    final SetOfStrings set;
    int idx = 0;

    SetOfStringsItr(SetOfStrings set) {
      this.set = set;
    }

    @Override
    public boolean hasMoreElements() {
      return set.busy.nextSetBit(idx) != -1;
    }

    @Override
    public String nextElement() {
      idx = set.busy.nextSetBit(idx);
      return elt[idx++];
    }
  }

  public void union (SetOfStrings s) {
    // TODO
    for (String elt : s.arrayFromSet())
      add(elt);
  }

  public void intersection(SetOfStrings s) {
    // TODO
    SetOfStrings newSet = new SetOfStrings();

    for (String elt : this.arrayFromSet()) {
      if (s.contains(elt))
        newSet.add(elt);
    }

    // CORRECTION
    for (String elt : this.arrayFromSet()) {
      if (!s.contains(elt))
        remove(elt);
    }
  }

  public int size() {
    return crtSize;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  private String[] arrayFromSet() {
    String[] t = new String[size()];
    int i=0;
    SetOfStringsItr itr = this.iterator();
    while (itr.hasMoreElements()) {
      t[i++] = itr.nextElement();
    }
    return t;
  }

  public String toString() { 
    SetOfStringsItr itr = this.iterator();
    if (isEmpty()) return "{}";
    String r = "{" + itr.nextElement();
    while (itr.hasMoreElements()) {
      r += ", " + itr.nextElement();
    }
    return r + "}";
  }
  // ------------------------------------------------------------
  // tiny demo
  // ------------------------------------------------------------
  public static void main(String [] args) {
    String a="abc";
    String b="defhijk";
    String c="hahaha";
    SetOfStrings s=new SetOfStrings();
    s.add(a); s.add(b); s.remove(a);
    if (s.contains(a) || s.contains(c) || !s.contains(b))
      System.out.println("bad news...");
    else 
      System.out.println("ok");
  }
}
